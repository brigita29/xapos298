package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Variants;

public interface VariantsRepository extends JpaRepository<Variants, Long>{

	// menggunakan JpaRepository
	List<Variants> findByIsActive(Boolean isActive);
	
	List<Variants> findByIsActiveAndCreatedBy(Boolean isActive, String user);
	
	// menggunakan Query Native
	@Query(value = "SELECT * FROM variants WHERE is_active = ?1  AND create_by =?2", nativeQuery = true)
	List<Variants> findByVariants(Boolean isActive, String orang);
	
	@Query(value = "SELECT * FROM variants v JOIN category c ON v.category_id = c.id WHERE active = ?1 AND is_Active = ?2", nativeQuery = true)
	List<Variants> findByVariant(Boolean active, Boolean isActive);
	
	@Query(value = "SELECT * FROM variants v JOIN category c ON v.category_id = c.id WHERE active = ?1 AND is_Active = ?2 AND c.id = ?3", nativeQuery = true)
	List<Variants> findByVarianCategoryId(Boolean categoryactive, Boolean variantsisActive, Long id);
	
	@Query(value = "SELECT * FROM variants WHERE(lower(name) LIKE lower(concat('%', ?1, '%')))", nativeQuery = true)
	List<Variants> searchByKey(String key);
}