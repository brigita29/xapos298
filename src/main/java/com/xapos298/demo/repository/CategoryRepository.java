package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	List<Category> findByIsActive(Boolean isActive);
	
	@Query(value = "SELECT * FROM category WHERE(lower(category_name) LIKE lower(concat('%', ?1, '%')))", nativeQuery = true)
	List<Category> searchByKey(String key);

}
