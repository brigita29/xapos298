package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findByIsActive(Boolean isActive);
	
	List<Product> findByIsActiveAndCreatedBy(Boolean isActive, String user);
	
	@Query(value = "SELECT * FROM product WHERE is_active = ?1 AND create_by =?2", nativeQuery = true)
	List<Product> findByProduct(Boolean isActive, String orang);
	
	@Query(value = "SELECT * FROM product p JOIN variants v ON p.variant_id = v.id WHERE active =?1 AND is_active = ?2", nativeQuery = true)
	List<Product> findByProduct(Boolean active, Boolean isActive);
	
	@Query(value = "SELECT * FROM product WHERE(lower(product_name) LIKE lower(concat('%', ?1, '%')))", nativeQuery = true)
	List<Product> searchByKey(String key);

}
