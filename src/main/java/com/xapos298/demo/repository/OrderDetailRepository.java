package com.xapos298.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xapos298.demo.model.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>{

	List<OrderDetail> findByHeaderId(Long id);
	
	//@Query(value = "SELECT * FROM order_detail WHERE header_id AND product_id NOT NULL", nativeQuery = true)
//	List<OrderDetail> findbyOrderDetails
	
}
