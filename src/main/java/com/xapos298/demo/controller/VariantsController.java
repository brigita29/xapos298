package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantsRepository;


@Controller
@RequestMapping("/variants/")
public class VariantsController {

	@Autowired
	private VariantsRepository variantsRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("indexvarapi")
	public ModelAndView indexvarapi() {
		ModelAndView view = new ModelAndView("variants/indexvarapi");
		List<Variants> listVariants = this.variantsRepository.findByIsActive(true);
		view.addObject("listVariants", listVariants);
		return view;
	}

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("variants/indexvarapi");
		List<Variants> listVariants = this.variantsRepository.findAll();
		view.addObject("listVariants", listVariants);
		return view;
	}

	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("variants/addform");
		Variants variants = new Variants();
		view.addObject("variants", variants);
		
		//mengambil list category
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory",listCategory);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variants variants, BindingResult result) {
		if (!result.hasErrors()) {
			if (variants.getId() != null) {
				Variants oldVariants = this.variantsRepository.findById(variants.getId()).orElse(null);
				variants.setCreatedBy(oldVariants.getCreatedBy());
				variants.setModifyDate(new Date());
				variants.setModifyBy("brigita");
				variants.setCreatedDate(oldVariants.getCreatedDate());
				this.variantsRepository.save(variants);
			} else {
				variants.setCreatedDate(new Date());
				variants.setCreatedBy("user1");
				this.variantsRepository.save(variants);
			}
			return new ModelAndView("redirect:/variants/index");
		} else {
			return new ModelAndView("redirect:/variants/index");
		}
	}

	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("variants/addform");
		Variants variants = this.variantsRepository.findById(id).orElse(null);
		
		view.addObject("variants",variants);
		return view;
	}

	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id) {
		if (id != null) {
			this.variantsRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/variants/index");
	}
}
