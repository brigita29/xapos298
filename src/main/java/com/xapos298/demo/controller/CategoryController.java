package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@Controller
@RequestMapping("/category/")
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("category/indexApi");
		List<Category> listCategory = this.categoryRepository.findAll();
		view.addObject("listCategory", listCategory);
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("category/index");
		List<Category> listCategory = this.categoryRepository.findAll();
		// System.out.println(listCategory.get(0).getCategoryName());
		view.addObject("listCategory", listCategory);
		return view;
	}

	@GetMapping("addform")
	public ModelAndView addForm() {
		ModelAndView view = new ModelAndView("category/addform");
		Category category = new Category();
		view.addObject("category", category);
		return view;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Category category, BindingResult result) {
//	System.out.println(category.getCategoryName());
//	return new ModelAndView("redirect:/category/index");
//		if (category.getId() != null) {
//			category.setCreatedBy("user1");
//			category.setCreatedDate(Date());
//			category.setModifyBy("user1");
//			category.setModifyDate(new Date());
//			System.out.println(category.getId());
//			this.categoryRepository.save(category);
//			return new ModelAndView("redirect:/category/index");
//		} else {
//			return new ModelAndView("redirect:/category/index");
//		}

		if (!result.hasErrors()) {
			if (category.getId() != null) {
				Category oldCategory =
						this.categoryRepository.findById(category.getId()).orElse(null);
						oldCategory.setModifyBy("brigita");
						oldCategory.setModifyDate(new Date());
			oldCategory.setCategoryInitial(category.getCategoryInitial());
			oldCategory.setCategoryName(category.getCategoryName());
						oldCategory.setIsActive(category.getIsActive());
						category = oldCategory;
			
			} else {
						category.setCreatedBy("user1");
						category.setCreatedDate(new Date()); // = date now / tanggal hari ini beserta hh:mm:ss
			}
			System.out.println(category.getId() + " " + category.getModifyBy() + " " + category.getCreatedBy());
			this.categoryRepository.save(category); // INSERT INTO .... VALUES ....
		}
			return new ModelAndView("redirect:/category/index");
		}
	
	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("category/addform");

		Category category = this.categoryRepository.findById(id).orElse(null); // SELECT * FROM CATEGORY WHERE = {id}

		view.addObject("category", category);
		return view;
	}
	
	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids") Long id) {
		if (id != null) {
			this.categoryRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/category/index");
	}
}
