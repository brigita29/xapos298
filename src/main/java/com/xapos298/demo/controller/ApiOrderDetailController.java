package com.xapos298.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.OrderDetail;
import com.xapos298.demo.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiOrderDetailController {

	@Autowired
	public OrderDetailRepository orderDetailRepositoty;
	
	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail) {
		
		OrderDetail orderDetailData = this.orderDetailRepositoty.save(orderDetail);
		
		if(orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<Object>("Save Item Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderdetailbyheaderid/{headerId}")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetail(@PathVariable ("headerId") Long id){
		try {
			List<OrderDetail> orderDetail = this.orderDetailRepositoty.findByHeaderId(id);
			return new ResponseEntity<>(orderDetail, HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
