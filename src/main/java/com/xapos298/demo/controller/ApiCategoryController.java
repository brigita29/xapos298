package com.xapos298.demo.controller;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.repository.CategoryRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiCategoryController {

	@Autowired
	public CategoryRepository categoryRepository;

	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> listCategory = this.categoryRepository.findByIsActive(true);
			return new ResponseEntity<List<Category>>(listCategory, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("category/add")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category) {

		category.setCreatedBy("user1");
		category.setCreatedDate(new Date());

		Category categoryData = this.categoryRepository.save(category);

		if (categoryData.equals(category)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("category/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Category> category = this.categoryRepository.findById(id);
			if (category.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/category/{id}")
	public ResponseEntity<Object> editCategory(@PathVariable("id") Long id, @RequestBody Category category) {

		Optional<Category> categoryData = this.categoryRepository.findById(id);

		if (categoryData.isPresent()) {
			category.setId(id);
			category.setModifyBy("user1");
			category.setModifyDate(Date.from(Instant.now()));
			category.setCreatedBy(categoryData.get().getCreatedBy());
			category.setCreatedDate(categoryData.get().getCreatedDate());
			this.categoryRepository.save(category);
			return new ResponseEntity<Object>("Updated Successfully", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/category/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id")Long id){
		Optional<Category> categoryData = this.categoryRepository.findById(id);
		
		if (categoryData.isPresent()) {
			
			Category category = new Category();
			category.setId(id);
			category.setIsActive(false);
			category.setModifyBy("user1");
			category.setModifyDate(new Date());
			category.setCreatedBy(categoryData.get().getCreatedBy());
			category.setCreatedDate(categoryData.get().getCreatedDate());
			category.setCategoryInitial(categoryData.get().getCategoryInitial());
			category.setCategoryName(categoryData.get().getCategoryName());
			this.categoryRepository.save(category);
			return new ResponseEntity<>("Deleted Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("category/search/{key}")
		public ResponseEntity<List<Category>> search(@PathVariable("key") String key){
			try {
				List<Category> searchCategory = this.categoryRepository.searchByKey(key);
				return new ResponseEntity<List<Category>>(searchCategory,HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
	}
}