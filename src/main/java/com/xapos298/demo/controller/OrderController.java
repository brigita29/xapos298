package com.xapos298.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Product;
import com.xapos298.demo.repository.ProductRepository;

@Controller
@RequestMapping("/order/")

public class OrderController {
	
	@Autowired
	private ProductRepository productRepository;

	@GetMapping("indexorapi")
	public ModelAndView indexorapi() {
		ModelAndView view = new ModelAndView("order/indexorapi");
		List<Product> listOrder = this.productRepository.findAll();
		view.addObject("listOrder",listOrder);
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("order/index");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listProduct",listProduct);
		return view;
	}
}