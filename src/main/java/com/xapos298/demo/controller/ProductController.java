package com.xapos298.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Product;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.ProductRepository;
import com.xapos298.demo.repository.VariantsRepository;

@Controller
@RequestMapping("/product/")

public class ProductController {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private VariantsRepository variantsRepository;
	
	@GetMapping("indexproapi")
	public ModelAndView indexproapi() {
		ModelAndView view = new ModelAndView("product/indexproapi");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listProduct",listProduct);
		return view;
	}
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("product/index");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listProduct",listProduct);
		return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addForm() {
		ModelAndView view = new ModelAndView("/product/addform");
		Product product = new Product();
		view.addObject("product",product);
		return view;
	
	//mengambil list variants
//	List<Variants> listVariants = this.variantsRepository.findAll();
//	view.addObject("listVariants",listVariants);
//	return view;
	
	//mengambil list category
//	List<Category> listCategory = this.categoryRepository.findAll();
//	view.addObject("listCategory",listCategory);
//	return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Product product, BindingResult result) {
		if (!result.hasErrors()) {
			if (product.getId() != null) {
				Product oldProduct = this.productRepository.findById(product.getId()).orElse(null);
				product.setCreatedBy(oldProduct.getCreatedBy());
				product.setModifyDate(new Date());
				product.setModifyBy("brigita");
				product.setCreatedDate(oldProduct.getCreatedDate());
				this.productRepository.save(product);
			} else {
				product.setCreatedDate(new Date());
				product.setCreatedBy("user1");
				this.productRepository.save(product);
			}
			return new ModelAndView("redirect:/product/index");
		} else {
			return new ModelAndView("redirect:/product/index");
		}
	}
	
	@GetMapping("edit/{ids}")
	public ModelAndView edit(@PathVariable("ids") Long id) {
		ModelAndView view = new ModelAndView("product/addform");
		Product product = this.productRepository.findById(id).orElse(null);
		
		view.addObject("product", product);
		return view;
	}
	
	@GetMapping("delete/{ids}")
	public ModelAndView delete(@PathVariable("ids")Long id) {
		if (id != null) {
			this.variantsRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/product/index");
	}
}