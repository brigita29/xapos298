package com.xapos298.demo.controller;


import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xapos298.demo.model.Category;
import com.xapos298.demo.model.Variants;
import com.xapos298.demo.repository.CategoryRepository;
import com.xapos298.demo.repository.VariantsRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiVariantsController {

	@Autowired
	public VariantsRepository variantsReposity;
	
	@Autowired 
	public CategoryRepository categoryRepository;

	@GetMapping("variants")
	public ResponseEntity<List<Variants>> getAllVariants() {
		try {
			List<Variants> listVariants = this.variantsReposity.findByIsActive(true);
			return new ResponseEntity<List<Variants>>(listVariants, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("variants/add")
	public ResponseEntity<Object> saveVariants(@RequestBody Variants variants) {

		variants.setCreatedBy("user1");
		variants.setCreatedDate(new Date());

		Variants variantsData = this.variantsReposity.save(variants);

		if (variantsData.equals(variants)) {
			return new ResponseEntity<>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("variants/{id}")
	public ResponseEntity<List<Variants>> getVariantsByIdEntity(@PathVariable("id") Long id) {
		try {
			Optional<Variants> variants = this.variantsReposity.findById(id);
			if (variants.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variants, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Variants>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/variants/{id}")
	public ResponseEntity<Object> editVariantsEntity(@PathVariable("id") Long id, @RequestBody Variants variants) {

		Optional<Variants> variantsData = this.variantsReposity.findById(id);

		if (variantsData.isPresent()) {
			variants.setCategoryId(variantsData.get().getCategoryId());
			variants.setId(id);
			variants.setModifyBy("user1");
			variants.setModifyDate(Date.from(Instant.now()));
			variants.setCreatedBy(variantsData.get().getCreatedBy());
			variants.setCreatedDate(variantsData.get().getCreatedDate());
			this.variantsReposity.save(variants);
			return new ResponseEntity<Object>("Updated Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("delete/variants/{id}")
	public ResponseEntity<Object> deleteVariants(@PathVariable("id") Long id) {
		Optional<Variants> variantsData = this.variantsReposity.findById(id);

		if (variantsData.isPresent()) {
			Variants variants = new Variants();
			variants.setId(id);
			variants.setIsActive(false);
			variants.setModifyBy("user1");
			variants.setModifyDate(new Date());
			variants.setCreatedBy(variantsData.get().getCreatedBy());
			variants.setCreatedDate(variantsData.get().getCreatedDate());
			variants.setVariantsInitial(variantsData.get().getVariantsInitial());
			variants.setVariantsNama(variantsData.get().getVariantsNama());
			this.variantsReposity.save(variants);
			return new ResponseEntity<>("Delete Successfuly", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("variantsByCategory/{id}")
	public ResponseEntity<List<Variants>> getVariantsByCategory(@PathVariable("id") Long id){
		try {
			List<Variants> variantsbycat= this.variantsReposity.findByVarianCategoryId(true, true, id);
			return new ResponseEntity<List<Variants>>(variantsbycat,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("search/variants/{key}")
	public ResponseEntity<List<Variants>> search(@PathVariable("key") String key){
		try {
			List<Variants> searchVariants = this.variantsReposity.searchByKey(key);
			return new ResponseEntity<List<Variants>>(searchVariants,HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("category/paging")
	public ResponseEntity<Map<String, Object>> getAllCategories(@RequestParam (defaultValue = "0") int page,
			@RequestParam(defaultValue = "5") int size) {
		try {
			List<Category> category = new ArrayList<>();
			Pageable pagingSort = PageRequest.of(page, size);
			
			Page<Category> pageTuts;
			pageTuts = this.categoryRepository.findAll(pagingSort);
			
			category = pageTuts.getContent();
			
			Map<String, Object> response = new HashMap<>();
			response.put("category", category);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPage", pageTuts.getTotalPages());
			
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
